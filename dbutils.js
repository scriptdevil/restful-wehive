var config = require('./config');

var dbutils = {};

function location(coords) {
  var lat = coords[0];
  var long = coords[1];
  return {type: "Point", coordinates: [long, lat]};
}

/* 
 DB-specific item generation.
*/
function make(itemfields, itemid) {
  console.log(itemfields);
  return {
    id: itemid,
    title: itemfields.title,
    price: itemfields.price,
    origprice: itemfields.origprice,
    // Precompute this field since it needs to be indexed.
    discount: itemfields.origprice - itemfields.price,
    age: itemfields.age,
    loc: location(itemfields.coords),
    // sane defaults for nullable optionals
    pics: itemfields.pics || [],
    description: itemfields.description || ""
  };
}

function makeUpdate(updatedFields) {
  var update = {};
  var field;
  for(field in updatedFields) {
    switch(field){
    case 'coords':
      update[field] = location(updatedFields[field]);
      break;
    default:
      update[field] = updatedFields[field];
      break;
    }
  }
  return update;
};

dbutils.addItem = function(connection, itemfields, callback) {
  var items = connection.collection(config.mongo.itemsCollection);
  var counters = connection.collection(config.mongo.countersCollection);

  var itemCounter = counters.findAndModify(
    {_id: config.mongo.itemsCounter},
    [],
    {$inc: {seq: 1}},
    {new: true},
    function(err, d) {
      // d returns the counter doc
      if(err) {
	callback(err, null);
      } else {
	var itemid = d.seq;
	var dbitem = make(itemfields, itemid);
	items.insert(dbitem, function(err, result) {
	  // result is the newly inserted item
	  if(err) {
	    callback(err, null);
	  } else {
	    callback(null, result[0].id);
	  }
	});
      }
    });
};

dbutils.bestDeals = function(connection, count, callback) {
  var items = connection.collection(config.mongo.itemsCollection);
  items.find(
    {},
    {sort: [['discount', -1]],
     limit: count}).toArray(function(err, d) {
       if(err) {
	 callback(err, null);
       } else {
	 callback(null, d);
       }
     });
};

dbutils.near = function(connection, coords, count, callback) {
  var items = connection.collection(config.mongo.itemsCollection);
  items.find({loc: { $near: { $geometry: location(coords)}}},
	     {limit: count})
    .toArray(function(err, d) { 
      if(err) {
	callback(err, null);
      } else {
	callback(null, d);
      }
    });
};

dbutils.get = function(connection, itemid, callback) {
  var items = connection.collection(config.mongo.itemsCollection);
  items.findOne({id: itemid}, function(err, d) {
    if(err) {
      callback(err, null);
    } else {
      callback(null, d);
    }
  });
};

dbutils.update = function(connection, itemid, updatedFields, callback) {
  var items = connection.collection(config.mongo.itemsCollection);
  var update = makeUpdate(updatedFields);
  console.log("Update");
  console.log(update);
  if(update['price'] || update['origprice']) {
    // Too bad. We have to use the slow-path now because of the
    // discount field
    items.findAndRemove({id: itemid}, function(err, d) {
      if(err) {
    	callback(err, null);
      } else {
    	if(d == null) {
    	  callback(null, null);
    	} else {
    	  var field;
    	  for(field in update) {
    	    // update fields inplace
    	    d[field] = update[field];
    	  }
    	  // Compute discount
    	  d.discount = d.origprice - d.price;

	  // Save this
	  items.insert(d, function(err, result) {
	  // result is the newly inserted item
	    if(err) {
	      callback(err, null);
	    } else {
	      callback(null, d);
	    }
	  });
    	}
      }
    });
  } else {
    var itemCounter = items.findAndModify(
      {id: itemid},
      [],
      {$set: update},
      {new: true},
      function(err, d) {
    	// d returns the counter doc
    	if(err) {
    	  callback(err, null);
    	} else {
    	  callback(null, d);
    	}
      });
  }
};

module.exports = dbutils;
