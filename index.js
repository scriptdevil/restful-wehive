var express = require('express');

var config = require('./config');
var utils = require('./utils');
var dbutils = require('./dbutils');

var app = express();

/* Connect to database */
var MongoClient = require('mongodb').MongoClient;
var connection = null;
var url = utils.generateMongoURL(config.mongo);
MongoClient.connect(url, function(err, database) {
  if(err) {
    console.log('Could not connect to DB at ' + url);
    process.exit(1);
  }
  connection = database;
  console.log('DB Connection successful');

  app.listen(config.express.port, function() {
    var items = connection.collection(config.mongo.itemsCollection);
    items.ensureIndex({discount: -1}, function(e, d) {
      items.ensureIndex({loc: "2dsphere"}, function(e, d) {
	console.log("Done indexing discount and locations");
	console.log('Listening on port ', config.express.port);
      });
    });
  });
});

/* Route handlers */
app.get('/', function(req, res) {
  res.set('Content-type', 'text/json');
  res.send( {ok: true});
});

app.get('/add', function(req, res) {
  res.set('Content-type', 'text/json');
  var item = utils.generateItem(req.query);
  if(item.status.ok) {
    dbutils.addItem(connection, item.fields, function(err, itemid) {
      if(err) {
	// TODO: Analyze err more deeply
	res.send({ok: false, problems: ['failed to add item to db']});
      } else {
	res.send({ok:true, id: itemid});
      }
    });
  } else {
    res.send(item.status);
  }
});

app.get('/bestdeals', function(req, res) {
  res.set('Content-type', 'text/json');

  /* The config argument is passed since it can instead be taken from
   a GET query parameter in the future */
  dbutils.bestDeals(connection, config.app.numdeals, function(err, items) {
    if(err) {
      res.send({ok: false, problems:["Couldn't get deals from DB"]});
    } else {
      res.send({ok: true, items: items});
    }
  });
});

app.get('/near', function(req, res) {
  res.set('Content-type', 'text/json');
  var latlong = utils.getLocation(req.query);
  if(latlong.status.ok) {
    dbutils.near(connection, latlong.coords, config.app.numnearest, function(err, items) {
      if(err) {
	res.send({ok: false, problems:["Couldn't get deals near the location"]});
      } else {
	res.send({ok: true, items: items});
      }
    });
  } else {
    res.send(latlong.status);
  }
});

app.get('/item/:id', function(req, res) {
  res.set('Content-type', 'text/json');
  var id = Number(req.params.id);
  dbutils.get(connection, id, function (err, item) {
    if(err) {
      res.send({ok: false, problems: ["couldn't find the item"]});
    } else {
      if(item) {
	res.send({ok: true, item: item});
      } else {
	res.send({ok: false, problems: ["no such item"]});
      }
    }
  });
});

app.get('/update/:id', function(req, res) {
  res.set('Content-type', 'text/json');
  var id = Number(req.params.id);

  var update = utils.getChanges(req.query);
  
  if(update.status.ok) {
    dbutils.update(connection, id, update.fields , function (err, item) {
      console.log("CALLED BACK");
      if(err) {
  	res.send({ok: false, problems: ["couldn't update"]});
      } else {
	if(item) {
	  res.send({ok: true, item: item});
	} else {
	  res.send({ok: false, problems: ["no such item"]});
	}
      }
    });
  } else {
    res.send(update.status);
  }
});

/* Cleanup isn't needed Mongo automatically closes connection when
 * process quits 
 */
