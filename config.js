var config = {};

/* express.js settings */
config.express = {};
config.express.port = 4483; // HIVE

/* MongoDB settings */
config.mongo = {};
config.mongo.server = "localhost";
config.mongo.port = 27017;
config.mongo.db = "wehive-test";
config.mongo.itemsCollection = "items";
config.mongo.countersCollection = "counters";
config.mongo.itemsCounter = "itemid";

/* Generic settings */
config.app = {};
config.app.numdeals = 20;
config.app.numnearest = 10;

module.exports = config;
