var utils = {};

utils.generateMongoURL = function(mongoconfig) {
  return 'mongodb://' + mongoconfig.server + ':' + mongoconfig.port + '/' + mongoconfig.db;
};

function checkField(fieldName, fieldValue) {
  var problem = null;
  switch(fieldName) {
  case 'title':
  case 'description':
    if(fieldValue == '') {
      problem = fieldName + " cannot be blank";
    }
    break;
  case 'price':
  case 'origprice':
  case 'age':
    if(isNaN(fieldValue)) {
      problem = fieldName + " should be a number";
    }
    if(fieldValue < 0) {
      problem = fieldName + " cannot be negative";
    }
    break;
  case 'coords':
    var lat = fieldValue[0];
    var long = fieldValue[1];
    if(isNaN(lat) || isNaN(long) ||
       lat < -90 || long < -180 ||
       lat > 90 || long > 180) {
       problem = fieldName + ' is not a valid location';
    }
    break;
  case 'pics':
    if(fieldValue.length == 0) {
      problem = fieldName + " cannot be an empty list";
    }
    break;
  default:
    problem = "unrecognized field: " + fieldName;
  }
  return problem;
}

function format(fieldName, fieldValue) {
  var value = null;
  switch(fieldName) {
  case 'title':
  case 'description':
    value = fieldValue.trim();
    break;
  case 'price':
  case 'origprice':
  case 'age':
    value = Number(fieldValue);
    break;
  case 'coords':
    var latlong = fieldValue.split(',');
    if(latlong.length == 2) {
      var lat = Number(latlong[0]);
      var long = Number(latlong[1]);
      value = [lat, long];
    } else {
      // fail while checking
      value = [NaN, NaN];
    }
    break;
  case 'pics':
    value = fieldValue.split(',')
      .map(function(pic) {
	return pic.trim();
      }).filter(function(pic) {
	return pic;
      });
  }

  return value;
}

utils.getChanges = function(query) {
  var fields = ['title', 'price', 'origprice', 'age',
		'coords', 'pics', 'description'];

  var update = {};
  update.status = {};
  update.status.problems = [];
  update.fields = {};
  update.status.ok = false;

  if(query['id']) {
    update.status.problems.push("ID field cannot be updated");
    return update;
  }
  fields.forEach(function(field) {
    if(query[field]) {
      update.fields[field] = format(field, query[field]);
    }
  });


  var updatedField;
  for(updatedField in update.fields) {
    var problem = checkField(updatedField, update.fields[updatedField]);
    if(problem) {
      update.status.problems.push(problem);
    }
  }
  if(update.status.problems.length == 0) {
    update.status.ok = true;
  }

  return update;
};

/*
 Takes the fields of a request and tries to generate the fields of a
 database item.
 */
utils.generateItem = function(query) {
  var requiredFields = ['title', 'price', 'origprice',
			'age', 'coords'];

  var optionalFields = ['pics', 'description'];  
  var item = {};
  item.status = {
    ok: false,
    missing: [],
    problems: []
  };
  item.fields = {};
  
  requiredFields.forEach(function(field) {
    if(query[field]){
      item.fields[field] = format(field, query[field]);
    } else {
      item.status.missing.push(field);
    }
  });
  
  optionalFields.forEach(function(field) {
    if(query[field]){
      item.fields[field] = format(field, query[field]);
    }
  });

  if(item.status.missing.length == 0) {
    var field;
    for(field in item.fields) {
      var problem = checkField(field, item.fields[field]);
      if(problem) {
	item.status.problems.push(problem);
      }
    }
    if(item.status.problems.length == 0) {
      item.status.ok = true;
    } 
  } else {
    item.status.ok = false;
    item.status.problems.push('Missing fields');
  }

  return item;
};

utils.getLocation = function(query) {
  var latlong = {};
  latlong.status = {};
  latlong.coords = [];
  var lat; var long;
  latlong.status.ok = false;
  latlong.status.missing = [];
  latlong.status.problems = [];
  if(query["lat"]) {
    lat = query["lat"];
  } else {
    latlong.status.missing.push("lat");
  }
  if(query["long"]) {
    long = query["long"];
  } else {
    latlong.status.missing.push("long");
  }

  if(latlong.status.missing.length != 0){
    latlong.status.problems.push("Missing fields");
  } else {
    
    lat = Number(lat);
    long = Number(long);
    var problem = checkField("coords", [lat, long]);

    if(problem) {
      latlong.status.problems.push(problem);
    } else {
      latlong.status.ok = true;
      latlong.coords = [lat, long];
    }
  }

  return latlong;
};
  
module.exports = utils;
