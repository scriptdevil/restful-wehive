var config = require('./config');
var utils = require('./utils');

var readline = require('readline');

var rl = readline.createInterface(process.stdin, process.stdout);

console.log("If you already have a data on the DB, this causes a tsunami, eats baby seals and shoots pandas. If you are certain you want to proceed, type YES (all caps)");
rl.setPrompt("destroy? ");

rl.on('line', function(l) {
  if(l == 'YES') {
    initDB();
    rl.close();
  } else {
    console.log("You did not type YES");
    console.log("Phew! That was close!");
    rl.close();
  }
});

rl.prompt();

function initDB() {
  var MongoClient = require('mongodb').MongoClient;
  var url = utils.generateMongoURL(config.mongo);
  
  MongoClient.connect(url, function(err, db) {
    if(err) {
      console.log('Could not connect to DB at ' + url);
      process.exit(1);
    }
    
    var counters = db.collection(config.mongo.countersCollection);
    counters.drop(
      function(e, d) {
	counters.insert(
	  {_id: config.mongo.itemsCounter, seq: 0},
	  function(e, d) {
	    if(e) { console.log("Failed to create item counter"); }
	    else {
	      var items = db.collection(config.mongo.itemsCollection);
	      items.drop(function() {
		// Ensure indices now
		// Descending index on discount
		items.ensureIndex({discount: -1}, function(e, d) {
		  if(e) { console.log("Error creating index on discount"); console.log(e); }
		  // Geospatial index on location
		  items.ensureIndex({loc: "2dsphere"}, function(e, d) {
		    if(e) { console.log("Error creating index on loc"); console.log(e); }
		    db.close();
		  });
		});
	      });
	    }
	  });
      });
  });
}


